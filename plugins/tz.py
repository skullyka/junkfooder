import plugin

from datetime import datetime
import pytz

def generate_abbrev_to_tz_map():
    abbrev_to_tz_map = {}
    for tz_name in pytz.all_timezones:
        try:
            tz = pytz.timezone(tz_name)
            abbrev = datetime.now(tz).strftime('%Z')
            abbrev_to_tz_map[abbrev] = tz_name
        except Exception as e:
            print(f"Error processing time zone {tz_name}: {e}")
    return abbrev_to_tz_map


def convert_time(time_str, from_tz, to_tz):
    """
    Convert a time from one time zone to another.

    Parameters:
    - time_str: a string representing the time (e.g., "17:00")
    - from_tz: a string representing the source time zone abbreviation (e.g., "PDT")
    - to_tz: a string representing the target time zone abbreviation (e.g., "CET")

    Returns:
    - A string representing the converted time in the target time zone and its relative day.
    """

    abbrev_to_tz_map = generate_abbrev_to_tz_map()
    # Convert abbreviations to IANA names
    from_tz = abbrev_to_tz_map.get(from_tz, from_tz)
    to_tz = abbrev_to_tz_map.get(to_tz, to_tz)

    # Use a fixed date (e.g., 2000-01-01) to make sure the comparison works correctly
    time_dt = datetime.strptime("2000-01-01 " + time_str, "%Y-%m-%d %H:%M")

    # Set the source time zone
    from_tz = pytz.timezone(from_tz)

    # Localize the datetime object to the source time zone
    time_dt = from_tz.localize(time_dt)

    original_date = time_dt.date()

    # Convert to the target time zone
    to_tz = pytz.timezone(to_tz)
    time_dt = time_dt.astimezone(to_tz)

    converted_date = time_dt.date()

    # Determine the day status
    day_status = ""
    if original_date == converted_date:
        day_status = "(same day)"
    elif original_date < converted_date:
        day_status = "(next day)"
    else:
        day_status = "(previous day)"

    # Format the converted time as a string
    converted_time_str = f"{time_dt.strftime('%H:%M')} {day_status}"

    return converted_time_str


def tz(irc, user, target, msg):
    items = msg.split(' ')
    items = [item for item in items if item != '']

    nick = user.partition('!')[0]

    if len(items) <= 1:  # no questions to answer...
        return

    answer = convert_time(items[1], items[2].upper(), items[3].upper())


    irc.msg(target, nick + ': ' + answer)


plugin.add_plugin('^!tz ', tz)
plugin.add_help('!tz',
                'Timezone. Usage: !tz 17:00 PDT CET')
